const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "index.html",
});

module.exports = {
  output: {
    publicPath: "/",
    filename: "js/main.[hash:8].js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: "babel-loader",
      },
      {
        test: /\.(png|jpg|gif|ico|svg|eot|ttf|woff|woff2)$/,
        loader: "url-loader",
        options: {
          limit: 25000,
          outputPath: "assets/",
          name: "[name].[hash:8].[ext]",
        },
      },
      {
        test: /\.(scss|sass|css)$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader",
          "postcss-loader",
          "sass-loader",
        ],
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx", ".json"],
  },
  plugins: [
    htmlPlugin,
    new MiniCssExtractPlugin({
      filename: "css/style.[hash:8].css",
    }),
  ],
  devServer: {
    historyApiFallback: {
      rewrites: [{ from: /^\/$/, to: "/index.html" }],
    },
  },
};
