import React, { Component } from "react";
import "./App.scss";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      assembled: [],
      expand: "",
    };

    this.socket = null;
    this.buffer = [];
    this.rafId = 0;
  }

  componentWillMount() {
    this.socket = io("http://94.72.145.6:1599");
    this.socket.on("connect", () => {
      this.socket.on("server-matches-all", (data) => {
        this.matchesAll(data);
      });

      this.socket.on("server-match-remove", (data) => {
        this.buffer.push({ m: "matchRemove", data });
      });

      this.socket.on("server-match-add", (data) => {
        this.buffer.push({ m: "matchAdd", data });
      });

      this.socket.on("server-match-update", (data) => {
        this.buffer.push({ m: "matchUpdate", data });
      });

      this.socket.on("server-odds-remove", (data) => {
        this.buffer.push({ m: "oddsRemove", data });
      });

      this.socket.on("server-odds-add", (data) => {
        this.buffer.push({ m: "oddsAdd", data });
      });

      this.socket.on("server-odds-update", (data) => {
        this.buffer.push({ m: "oddsUpdate", data });
      });
    });
  }

  expand = async (id) => {
    if (this.state.expand !== id) {
      this.setState({ expand: id });
    } else {
      this.setState({ expand: "" });
    }
  };

  assemble = async (data) => {
    let assembled = [...this.state.assembled];

    for (let i = 0; i < data.length; i++) {
      let {
        id,
        sportId,
        sportName,
        leagueId,
        leagueName,
        home,
        away,
        startDate,
        updateDate,
        oc,
      } = data[i];

      let sportIndex = this.inArray("sportId", sportId, assembled);
      if (sportIndex === false) {
        assembled.push({ sportId, sportName, leagues: [] });
        sportIndex = this.inArray("sportId", sportId, assembled);
      }

      let leagueIndex = this.inArray(
        "leagueId",
        leagueId,
        assembled[sportIndex].leagues
      );
      if (leagueIndex === false) {
        assembled[sportIndex].leagues.push({
          leagueId,
          leagueName,
          matches: [],
        });
        leagueIndex = this.inArray(
          "leagueId",
          leagueId,
          assembled[sportIndex].leagues
        );
      }

      let matchIndex = this.inArray(
        "id",
        id,
        assembled[sportIndex].leagues[leagueIndex].matches
      );
      if (matchIndex === false) {
        assembled[sportIndex].leagues[leagueIndex].matches.push({
          id,
          home,
          away,
          startDate,
          updateDate,
          oc,
        });
      } else {
        assembled[sportIndex].leagues[leagueIndex].matches[
          matchIndex
        ].updateDate = updateDate;
      }
    }

    for (let i = assembled.length - 1; i >= 0; i--) {
      let { leagues } = assembled[i];

      for (let j = leagues.length - 1; j >= 0; j--) {
        let { matches } = leagues[j];

        for (let x = matches.length - 1; x >= 0; x--) {
          let { id } = matches[x];

          let matchIndex = this.inArray("id", id, data);
          if (matchIndex === false) {
            assembled[i].leagues[j].matches.splice(x, 1);
          }
        }

        if (assembled[i].leagues[j].matches.length === 0) {
          assembled[i].leagues.splice(j, 1);
        } else {
          assembled[i].leagues[j].matches.sort((a, b) =>
            a.startDate > b.startDate ? 1 : -1
          );
        }
      }

      if (assembled[i].leagues.length === 0) {
        assembled.splice(i, 1);
      } else {
        assembled[i].leagues.sort((a, b) =>
          a.leagueName > b.leagueName ? 1 : -1
        );
      }
    }

    assembled.sort((a, b) => (a.sportName > b.sportName ? 1 : -1));

    this.setState({ assembled });
  };

  operation = async () => {
    if (this.buffer.length > 0) {
      let { m, data } = this.buffer[0];

      await this[m](data);
      this.buffer.splice(0, 1);
    }

    this.rafId = window.requestAnimationFrame(this.operation);
  };

  inArray = (key, value, array) => {
    for (let i = 0; i < array.length; i++) {
      if (array[i][key] === value) {
        return i;
      }
    }
    return false;
  };

  matchesAll = async (data) => {
    await this.setState({ data });
    await this.assemble(data);
    this.operation();
  };

  matchRemove = async (id) => {
    let index = this.inArray("id", id, this.state.data);
    if (index !== false) {
      let data = [...this.state.data];
      data.splice(index, 1);
      await this.setState({ data });
      await this.assemble(data);
    }
  };

  matchAdd = async (match) => {
    let index = this.inArray("id", match.id, this.state.data);
    if (index === false) {
      let data = [...this.state.data];
      data.push(match);
      await this.setState({ data });
      await this.assemble(data);
    }
  };

  matchUpdate = async (match) => {
    let index = this.inArray("id", match.id, this.state.data);
    if (index !== false) {
      let data = [...this.state.data];
      data[index].updateDate = match.updateDate;
      await this.setState({ data });
      await this.assemble(data);
    }
  };

  oddsRemove = async (bet) => {
    try {
      let index = this.inArray("id", bet.id, this.state.data);
      let ocIndex = this.inArray("ocId", bet.ocId, this.state.data[index].oc);
      let otIndex = this.inArray(
        "otId",
        bet.otId,
        this.state.data[index].oc[ocIndex].ot
      );

      let data = [...this.state.data];
      data[index].oc[ocIndex].ot[otIndex].value = bet.value;
      data[index].oc[ocIndex].ot[otIndex].caret = bet.value;
      data[index].oc[ocIndex].ot[otIndex].removed = true;
      await this.setState({ data });
      await this.assemble(data);
    } catch (err) {}
  };

  oddsAdd = async (bet) => {
    try {
      let index = this.inArray("id", bet.id, this.state.data);
      let ocIndex = this.inArray("ocId", bet.ocId, this.state.data[index].oc);
      let otIndex = this.inArray(
        "otId",
        bet.otId,
        this.state.data[index].oc[ocIndex].ot
      );

      let data = [...this.state.data];
      data[index].oc[ocIndex].ot[otIndex].value = bet.value;
      data[index].oc[ocIndex].ot[otIndex].caret = bet.caret;
      data[index].oc[ocIndex].ot[otIndex].removed = false;
      await this.setState({ data });
      await this.assemble(data);
    } catch (err) {}
  };

  oddsUpdate = async (bet) => {
    try {
      let index = this.inArray("id", bet.id, this.state.data);
      let ocIndex = this.inArray("ocId", bet.ocId, this.state.data[index].oc);
      let otIndex = this.inArray(
        "otId",
        bet.otId,
        this.state.data[index].oc[ocIndex].ot
      );

      let data = [...this.state.data];
      data[index].oc[ocIndex].ot[otIndex].value = bet.value;
      data[index].oc[ocIndex].ot[otIndex].caret = bet.caret;
      await this.setState({ data });
      await this.assemble(data);
    } catch (err) {}
  };

  render() {
    return (
      <div className="app">
        <div className="view">
          <div className="sports">
            {this.state.assembled.map((sport) => (
              <div className="sport" key={"sport-" + sport.sportId}>
                <div className="sport-name">{sport.sportName}</div>
                <div className="leagues">
                  {sport.leagues.map((league) => (
                    <div className="league" key={"league-" + league.leagueId}>
                      <div className="league-name">{league.leagueName}</div>
                      <div className="matches">
                        <div className="match-head">
                          <div className="match-id">Event ID</div>
                          <div className="match-home">Home</div>
                          <div className="match-away">Away</div>
                          <div className="match-start-date">Start Time</div>
                          <div className="match-update-date">Update Time</div>
                          <div className="def-bets">
                            <div className="ot-name">1</div>
                            <div className="ot-name">x</div>
                            <div className="ot-name">2</div>
                          </div>
                          <div className="more"></div>
                        </div>
                        {league.matches.map((match) => (
                          <div
                            className={
                              "match" +
                              (this.state.expand === match.id ? " expand" : "")
                            }
                            key={"match-" + match.id}
                          >
                            <div className="top">
                              <div className="match-id">{match.id}</div>
                              <div className="match-home">{match.home}</div>
                              <div className="match-away">{match.away}</div>
                              <div className="match-start-date">
                                {moment(match.startDate).format(
                                  "YYYY-MM-DD HH:mm:ss"
                                )}
                              </div>
                              <div
                                className="match-update-date"
                                title={moment(match.updateDate).format(
                                  "YYYY-MM-DD HH:mm:ss"
                                )}
                              >
                                {moment(match.updateDate).fromNow()}
                              </div>
                              <div className="ots">
                                {match.oc[0].ot.map((ot) => (
                                  <div
                                    className="ot"
                                    key={"def-bets-" + match.id + ot.otId}
                                  >
                                    <div
                                      className={
                                        "ot-value removed-" +
                                        ot.removed +
                                        " caret-" +
                                        ot.caret
                                      }
                                    >
                                      {ot.value}
                                    </div>
                                  </div>
                                ))}
                              </div>
                              <div
                                className="more"
                                onClick={() => this.expand(match.id)}
                              >
                                {this.state.expand === match.id ? "-" : "+"}
                              </div>
                            </div>
                            {this.state.expand === match.id && (
                              <div className="bottom">
                                <div className="eots">
                                  {match.oc
                                    .filter((oc, index) => index > 0)
                                    .map((oc) => (
                                      <div
                                        className={"eoc eoc-" + oc.ocId}
                                        key={
                                          "expanded-head-bets-" +
                                          match.id +
                                          oc.ocId
                                        }
                                      >
                                        <div className="oc-name">{oc.name}</div>
                                        <div className="ots">
                                          <div
                                            className={
                                              "heading c-" + oc.ot.length
                                            }
                                          >
                                            {oc.ot.map((ot) => (
                                              <div
                                                className="hot"
                                                key={
                                                  "def-hbets-" +
                                                  match.id +
                                                  ot.otId
                                                }
                                              >
                                                <div className="ot-name">
                                                  {ot.name}
                                                </div>
                                              </div>
                                            ))}
                                          </div>
                                          <div
                                            className={"body c-" + oc.ot.length}
                                          >
                                            {oc.ot.map((ot) => (
                                              <div
                                                className="bot"
                                                key={
                                                  "def-bbets-" +
                                                  match.id +
                                                  ot.otId
                                                }
                                              >
                                                <div
                                                  className={
                                                    "ot-value removed-" +
                                                    ot.removed +
                                                    " caret-" +
                                                    ot.caret
                                                  }
                                                >
                                                  {ot.value}
                                                </div>
                                              </div>
                                            ))}
                                          </div>
                                        </div>
                                      </div>
                                    ))}
                                </div>
                              </div>
                            )}
                          </div>
                        ))}
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
